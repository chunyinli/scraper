var express = require('express');
var config = require('../config/config.js');
var scraper = require('../controllers/scraper.js');

var router = express.Router();


router.get('/', function(req, res, next) {
    var scraperPromise = scraper.start();
        
        scraperPromise.then(function (data) {
            res.send(data);
        });
});

module.exports = router;
