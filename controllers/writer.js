var fs = require('fs');


var writer = {
    write: function (data) {
        console.log(data);
        
        fs.writeFile('public/json/players.json', JSON.stringify({
            players: data
        }), function (err) {
            if (err) {
                console.log('Error ocurred while writing JSON file: ' + err);
            }
        });
    }

};

module.exports = writer;
