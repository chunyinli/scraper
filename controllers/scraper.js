var express = require('express');
var request = require('request');
var querystring = require('querystring');
var _ = require('lodash');


var config = require('../config/config.js');

var router = express.Router();

// main promise array
var promiseArr = [];
var noResultCount = 0;

function getPlayerName (html) {
    var result;
    var match;

    match = html.match(config.titleRegex)[0];

    if (match) {
        // since we are using '<title>' and '-' to match player's name
        // remove those unneeded characters after player name is found
        result = match.substring(match.indexOf('>') + 1, match.indexOf('-')).trim();

        // if it's an empty string that means there was no player name
        // in the title to begin with
        if (!result) {
            result = null;
        }
    } else {

        // can't find title tag
        result = null;
    }
    return result;
}

/**
 * Make request to player page
 * @param  {[type]} playerId [description]
 * @return {[type]}          [description]
 */
function requestPlayerPage (playerId) {
    var playerUrl = config.baseUrl + playerId;
    var fullbody = '';

    var promise = new Promise(function (resolve, reject) {

        console.log(playerUrl);

        request(playerUrl)
            .on('data', function (chunk) {
                var html = chunk.toString();
                var playerName;

                fullbody += html;

                // if title closing tag has been loaded, abort request
                // and extract player name
                if (config.titleClosedRegex.test(fullbody)) {
                    this.abort();

                    playerName = getPlayerName(fullbody);

                    if (playerName) {
                        resolve({
                            playerId: playerId,
                            playerName: playerName
                        });
                    } else {
                        // can't find name in title tag
                        resolve(null);
                    }
                    
                }

                // if head closing tag has been loaded without finding title tag, abort request
                if (config.headClosedRegex.test(fullbody)) {
                    this.abort();

                    resolve(null);
                }

            })
            .on('end', function () {
                resolve(null);
            });

    });

    return promise.then(function (result) {

        // if none found, add to count
        if (result === null) {
            noResultCount++;
        }

        return result;
    });
}

function postProcess (arr) {
    //remove all falsy values (null)
    return _.compact(arr);
}

function flowControl (playerId, mainPromise) {

    // end flow when noReaultCount is over torlerance level, or
    // when all player page has been requested
    if (noResultCount > config.noResultTorlerance || playerId > config.endingId) {
        endFlow(mainPromise);

        return;
    }

    promiseArr.push(requestPlayerPage(playerId));

    playerId++;

    setTimeout(function () {
        flowControl(playerId, mainPromise);
    }, 1000);
}

// create promise for arr when flow is done 
function endFlow (mainPromise) {
    var endPromise = Promise.all(promiseArr)
        .then(function (valuesArr) {
            // write to file here
            console.timeEnd('scraper');
            return postProcess(valuesArr);
        })
        .catch(function (reason) {
            console.log(reason);
            return reason;
        });

    mainPromise.resolve(endPromise);
}

var scraper = {
    start: function () {
        console.time('scraper');
        var scraperPromise;
        
        // reset promise array and no-result count
        promiseArr = [];
        noResultCount = 0;

        scraperPromise = new Promise(function (resolve, reject) {
            var mainPromise = {
                resolve: resolve,
                reject: reject
            };

            flowControl(config.startingId, mainPromise);
        });

        return scraperPromise;

    }
};

module.exports = scraper;
