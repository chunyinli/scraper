// Main Config Object

module.exports = {
    baseUrl: 'http://www.rotoworld.com/player/nfl/',
    startingId: 11197,
    endingId: 11250,

    // Searches for string beging with '<title' and ends with ' -'
    // everything in-between can be anything except '<'
    titleRegex: /(<title)([\s\S][^<]*?)(\s-)/i,
    titleClosedRegex: /(<\/title>)/i,
    headClosedRegex: /(<\/head>)/i,

    // how many no result search to tolerate
    noResultTorlerance: 5
};
